package com.example.designux;

import java.util.*;
import java.io.*;

import org.xmlpull.v1.*;

import android.util.Xml;

public class XMLParser {
	List<UserProfile> users;
	private UserProfile user;
	public XMLParser(){
		users = new ArrayList<UserProfile>();
	}
	public List<UserProfile> getUsers(){
		return users;
	}
	public List<UserProfile> parse(InputStream isr){
	
		XmlPullParser parser = Xml.newPullParser();
		try {
		    
		    // auto-detect the encoding from the stream
		    parser.setInput(isr,null);
		    int eventType = parser.getEventType();
		    boolean done = false;
		    while (eventType != XmlPullParser.END_DOCUMENT && !done){
		        String name = null;
		        switch (eventType){
		            case XmlPullParser.START_DOCUMENT:
		                break;
		            case XmlPullParser.START_TAG:
		                name = parser.getName();
		                if (name.equalsIgnoreCase("slot")){
		                    user = new UserProfile();
		                }
		                break;
		            case XmlPullParser.END_TAG:
		                name = parser.getName();
		                if (name.equalsIgnoreCase("slot") && 
		                   user != null){
		                    users.add(user);
		                } else if (name.equalsIgnoreCase("name")){
		                    user.setName(name);		                    
		                } else if (name.equalsIgnoreCase("age")){
		                	user.setAge(Integer.parseInt(name));
		                } else if (name.equalsIgnoreCase("son")){
		                	user.setSon(Integer.parseInt(name));
		                } else if (name.equalsIgnoreCase("daughter")){
		                	user.setDaughter(Integer.parseInt(name));
		                }
		                break;
		            default: break;
		            }
		        eventType = parser.next();
		        }
		} catch (FileNotFoundException e) {
		    // TODO
		} catch (IOException e) {
		    // TODO
		} catch (Exception e){
		    // TODO
		}
		return users;
	}
}
