package com.example.designux;

public class UserProfile {

	private String name;
	private int age;
	private int son;
	private int daughter;
	
	public void setName(String name_)
	{
		this.name = name_;
	}
	public void setAge(int age_)
	{
		this.age = age_;
	}
	public void setSon(int son_)
	{
		this.son = son_;		
	}
	public void setDaughter(int daughter_)
	{
		this.daughter = daughter_;
	}
	public String name()
	{
		return this.name;
	}
	public int age()
	{
		return this.age;	
	}
	public int son()
	{
		return this.son;
	}
	public int daughter()
	{
		return this.daughter;
	}
}
